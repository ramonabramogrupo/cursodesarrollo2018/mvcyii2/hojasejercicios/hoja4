<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'formulario2',
    'options' => ['class' => 'form-horizontal'],
]); 
?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'nombre') ?>
    <?= $form->field($model, 'apellidos') ?>
    <?= $form->field($model, 'telefono') ?>
    <?= $form->field($model, 'correo') ?>
    <?= $form
        ->field($model, 'casado')
        ->checkbox([
            'uncheck'=>"No",
            "value"=>"Si",
        ]);
    ?>
    <?= $form
        ->field($model, 'poblacion')
        ->dropDownList($model->getValores(), [
                "prompt"=>"Seleccione una poblacion",
            ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    </div>
    </div>
<?php ActiveForm::end() ?>

