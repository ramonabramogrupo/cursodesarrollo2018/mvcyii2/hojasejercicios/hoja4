<?= 
\yii\widgets\DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'apellidos',
            'telefono',
            'correo',
            'poblacion',
            'casado',
        ],
    ]); 
?>