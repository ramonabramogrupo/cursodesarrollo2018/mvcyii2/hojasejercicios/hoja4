﻿--
-- Definition for database hoja4yii2
--
DROP DATABASE IF EXISTS hoja4yii2;
CREATE DATABASE IF NOT EXISTS hoja4yii2
	CHARACTER SET latin1
	COLLATE latin1_swedish_ci;

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

-- 
-- Set default database
--
USE hoja4yii2;

--
-- Definition for table alumnos
--
CREATE TABLE IF NOT EXISTS alumnos (
  nif VARCHAR(255) NOT NULL,
  nombre VARCHAR(255) DEFAULT NULL,
  apellidos VARCHAR(255) DEFAULT NULL,
  telefono VARCHAR(255) DEFAULT NULL,
  fechaNacimiento DATE DEFAULT NULL,
  direccion VARCHAR(255) DEFAULT NULL,
  poblacion VARCHAR(255) DEFAULT NULL,
  provincia VARCHAR(255) DEFAULT 'Cantabria',
  email VARCHAR(50) DEFAULT NULL,
  foto VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (nif),
  UNIQUE INDEX UK_alumnos_email (email)
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

--
-- Definition for table oposiciones
--
CREATE TABLE IF NOT EXISTS oposiciones (
  id INT(11) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(255) DEFAULT NULL,
  organismo VARCHAR(255) DEFAULT NULL,
  escala VARCHAR(255) DEFAULT NULL,
  año VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

--
-- Definition for table pagos
--
CREATE TABLE IF NOT EXISTS pagos (
  id INT(11) NOT NULL AUTO_INCREMENT,
  preparan INT(11) DEFAULT NULL,
  fechaPago DATE DEFAULT NULL,
  importe FLOAT DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX UK_pagos (preparan, fechaPago),
  CONSTRAINT FK_pagos_preparan FOREIGN KEY (preparan)
    REFERENCES preparan(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

--
-- Definition for table preparan
--
CREATE TABLE IF NOT EXISTS preparan (
  id INT(11) NOT NULL AUTO_INCREMENT,
  alumno VARCHAR(255) DEFAULT NULL,
  oposicion INT(11) DEFAULT NULL,
  fechaalta DATE DEFAULT NULL,
  fechabaja DATE DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX UK_preparan (alumno, oposicion, fechaalta),
  CONSTRAINT FK_preparan_alumno FOREIGN KEY (alumno)
    REFERENCES alumnos(nif) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_preparan_oposicion FOREIGN KEY (oposicion)
    REFERENCES oposiciones(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

-- 
-- Dumping data for table alumnos
--

-- Table hoja4yii2.alumnos does not contain any data (it is empty)

-- 
-- Dumping data for table oposiciones
--

-- Table hoja4yii2.oposiciones does not contain any data (it is empty)

-- 
-- Dumping data for table pagos
--

-- Table hoja4yii2.pagos does not contain any data (it is empty)

-- 
-- Dumping data for table preparan
--

-- Table hoja4yii2.preparan does not contain any data (it is empty)

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;