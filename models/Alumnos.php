<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property string $nif
 * @property string $nombre
 * @property string $apellidos
 * @property string $telefono
 * @property string $fechaNacimiento
 * @property string $direccion
 * @property string $poblacion
 * @property string $provincia
 * @property string $email
 * @property string $foto
 *
 * @property Preparan[] $preparans
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nif'], 'required'],
            [['fechaNacimiento'], 'safe'],
            [['nif', 'nombre', 'apellidos', 'telefono', 'direccion', 'poblacion', 'provincia', 'foto'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 50],
            [['email'], 'unique'],
            [['nif'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nif' => 'Nif',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'telefono' => 'Telefono',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'direccion' => 'Direccion',
            'poblacion' => 'Poblacion',
            'provincia' => 'Provincia',
            'email' => 'Email',
            'foto' => 'Foto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreparans()
    {
        return $this->hasMany(Preparan::className(), ['alumno' => 'nif']);
    }
}
