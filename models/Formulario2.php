<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Formulario2 extends Model
{
    public $nombre;
    public $apellidos;
    public $telefono;
    public $correo;
    public $poblacion;
    private $valores=[
        "Santander"=>"Santander",
        "Potes"=>"Potes",
        "Laredo"=>"Laredo"
        ];
    public $casado;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['nombre', 'correo'], 'required'],
            ['nombre','string','max'=>10],
            ['correo', 'email'],
            ['poblacion','in','range'=> array_keys($this->valores)],
            ['telefono','required','when'=>function($model){
            return $model->correo=='';
            },'whenClient' => "function (attribute, value) {
                return $('#formulario2-correo').val() =='';
            }"],
            ['casado','boolean','trueValue'=>"Si",'falseValue'=>"No"],
            [['nombre','apellidos','telefono','correo','poblacion','casado'],'safe']
        ];
    }

    function getValores() {
        return $this->valores;
    }

        /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
        ];
    }
       
}
