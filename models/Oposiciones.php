<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "oposiciones".
 *
 * @property int $id
 * @property string $nombre
 * @property string $organismo
 * @property string $escala
 * @property string $año
 *
 * @property Preparan[] $preparans
 */
class Oposiciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'oposiciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'organismo', 'escala', 'año'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'organismo' => 'Organismo',
            'escala' => 'Escala',
            'año' => 'Año',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreparans()
    {
        return $this->hasMany(Preparan::className(), ['oposicion' => 'id']);
    }
}
