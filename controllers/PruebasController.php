<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Formulario2;
use Yii;

/**
 * Description of pruebasController
 *
 * @author ramon
 */
class PruebasController extends Controller{
    public function actionIndex($id){
        if($id){
            $mensaje=$id;
        }else{
            $mensaje="Probando un poco Yii2";
        }
        
        return $this->render("index",[
            "mensaje"=>$mensaje,
        ]);
    }
    
    public function actionFormu1(){
        $datos=Yii::$app->request->post();
        if($datos){
            $this->redirect(["index","id"=>$datos["mensaje"]]);
        }
        
        return $this->render("formu1");
    }
    
    public function actionFormu2(){
        $modelo=new Formulario2();
        $datos=Yii::$app->request->post();
        
        if($datos){
            $modelo->load($datos);
            if($modelo->validate()){
                return $this->render("resultados",[
                    "model"=>$modelo,
                ]);
            } 
        }
        
               
       
        return $this->render("formu2",[
            "model"=>$modelo,
            ]);
    }
}
